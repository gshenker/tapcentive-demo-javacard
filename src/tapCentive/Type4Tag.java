package tapCentive;

import javacard.framework.APDU;
import javacard.framework.Applet;
import javacard.framework.ISO7816;
import javacard.framework.ISOException;
import javacard.framework.Util;

public class Type4Tag extends Applet { 
	
	private byte[] capabilityContainer;
	public static byte[] NDEF;
	private byte selectedFile; 
	
	public static void install(byte[] bArray, short bOffset, byte bLength) 
	{
		
		new Type4Tag(bArray, bOffset, bLength); 
	} 
	
	public Type4Tag(byte[] bArray, short bOffset, byte bLength)  
	{
		
		capabilityContainer = new byte[(short) 15];
		capabilityContainer[0] = (byte)0x00;   
		capabilityContainer[1] = (byte)0x0f;   // CC LEN
		capabilityContainer[2] = (byte)0x20;  // mapping version
		capabilityContainer[3] = (byte)0x00;
		capabilityContainer[4] = (byte)0xcb;   // max read
		capabilityContainer[5] = (byte)0x00;
		capabilityContainer[6] = (byte)0xc4;    // max write
		capabilityContainer[7] = (byte)0x04;    // Tag for 1st TLV
		capabilityContainer[8] = (byte)0x06;	// Length for TLV	
		capabilityContainer[9] = (byte)0xe1;	// File ID
		capabilityContainer[10] = (byte)0x04;	// File ID
		capabilityContainer[11] = (byte)0x00;
		capabilityContainer[12] = (byte)0xc2;	// max file size
		capabilityContainer[13] = (byte)0x00;	// read conditions
		capabilityContainer[14] = (byte)0xFF;	// write conditions
		
		NDEF = new byte[(short) 194];
		NDEF[0] = (byte)0x00;
		NDEF[1] = (byte)0x1B;   // length of subsequent NDEF data. Calculated as payload + 4
		NDEF[2] = (byte)0xD1;	// One message (Message Begin and Message End set) / no chunk / short record / no identification field / NFC well known type
		NDEF[3] = (byte)0x01;	// type (index 5) is 1 byte long
		NDEF[4] = (byte)0x17;   // length of payload.  index 6 onwards. Payload is the URL received in tag 9f38 therefore received data object length
		NDEF[5] = (byte)0x55;	// Payload type - "U" for URI
		NDEF[6] = (byte)0x03;	// 1st byte of the payload. Payload identification ("http://")
		NDEF[7] = (byte)0x31;	// 
		NDEF[8] = (byte)0x33;
		NDEF[9] = (byte)0x35;
		NDEF[10] = (byte)0x36;
		NDEF[11] = (byte)0x6d;
		NDEF[12] = (byte)0x6f;
		NDEF[13] = (byte)0x62;
		NDEF[14] = (byte)0x69;
		NDEF[15] = (byte)0x6c;
		NDEF[16] = (byte)0x65;
		NDEF[17] = (byte)0x2e;
		NDEF[18] = (byte)0x63;
		NDEF[19] = (byte)0x6f;
		NDEF[20] = (byte)0x6d;
		NDEF[21] = (byte)0x2f;
		NDEF[22] = (byte)0x77;
		NDEF[23] = (byte)0x65;
		NDEF[24] = (byte)0x6c;
		NDEF[25] = (byte)0x63;
		NDEF[26] = (byte)0x6f;
		NDEF[27] = (byte)0x6d;
		NDEF[28] = (byte)0x65;		
		
		register();
	}

	public void process(APDU apdu) {
		byte[] apduBuffer = apdu.getBuffer();
	
		// Good practice: Return 9000 on SELECT
		if (selectingApplet()) {
			return;
		}
	
		// Select
		if(apduBuffer[ISO7816.OFFSET_CLA]  == (byte)0x00 && apduBuffer[ISO7816.OFFSET_INS]  == (byte)0xa4)
		{
			apdu.setIncomingAndReceive();
			if (apduBuffer[5] == (byte)0xE1 && apduBuffer[6] == (byte)0x03 ){
				selectedFile = 3;
			}
			if (apduBuffer[5] == (byte)0xE1 && apduBuffer[6] == (byte)0x04 ){
				selectedFile = 4;
			}
			return;
		}
		
		
		if(apduBuffer[ISO7816.OFFSET_CLA]  == (byte)0x00 && apduBuffer[ISO7816.OFFSET_INS]  == (byte)0xb0)
		{
			// Read CC
			if( selectedFile == 3 ){
				Util.arrayCopyNonAtomic(capabilityContainer, (short)0, apduBuffer, (short)0, (short) 15 );
				apdu.setOutgoingAndSend((short)0, (short) 15);
				return;
			}
			// Read NDEF
			if( selectedFile == 4 ){
				// TODO: put in a check here in case Le is 00.
				short lengthToRead = Util.makeShort((byte)0, apduBuffer[4]);
				short offset = Util.makeShort((byte)0, apduBuffer[3]);
				if((short)(offset+lengthToRead) > 194 || lengthToRead == 0 )
					lengthToRead = (short)(194-offset);
				Util.arrayCopy(NDEF, (short)offset, apduBuffer, (short)0, (short) lengthToRead );
				apdu.setOutgoingAndSend((short)0, (short) lengthToRead);
				return;
			}
		}
		
		if(apduBuffer[ISO7816.OFFSET_CLA]  == (byte)0x00 && apduBuffer[ISO7816.OFFSET_INS]  == (byte)0xd6)
		{
						ISOException.throwIt(ISO7816.SW_FUNC_NOT_SUPPORTED); 

		}
			// good practice: If you don't know the INStruction, say so:
			ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
	}
}
