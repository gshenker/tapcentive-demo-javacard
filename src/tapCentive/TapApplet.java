/**
 * 
 */
package tapCentive;    
 
import javacard.framework.APDU;
import javacard.framework.ISO7816;
import javacard.framework.Applet;
import javacard.framework.ISOException;
import javacard.framework.JCSystem;
import javacard.framework.Util;
import javacard.security.DESKey;
import javacard.security.KeyBuilder;
import javacard.security.RandomData;
import javacard.security.Signature;
import javacardx.crypto.Cipher;
import tapCentive.Type4Tag;
   
/** 
 * @author gavin_000
 * Product version of TapCentive. This version excludes any game specific logic. It is built upon the original demonstration version committed
 * to bitbucket on 8/21/2013
 *  
 */

// TODO make sure that status words are representative of what the problem actually is.
public class TapApplet extends Applet { 
	
	// APDU Instruction bytes
	final static byte  INS_PROCESS_SELECT 	= (byte) 0xa4;    // 
	final static byte  INS_INFORMATION    	= (byte) 0x20;	// Receive information from Consumer app and respond
	final static byte  INS_RETRIEVE		  	= (byte) 0x24;	// Server interaction - retrieving data
	final static byte  INS_UPDATE 		  	= (byte) 0x26;	// Server interaction - updating
	final static byte  INS_READ 		  	= (byte) 0xB0;	// Read image and descriptions
	final static byte  INS_GET_RESPONSE    	= (byte) 0xC0; // Retrieve information that could not fit within a single apdu
	final static byte  TYPE_CAMPAIGN    	= (byte) 0x6A;	// Campaign data type
	final static byte  TYPE_IMAGE		  	= (byte) 0x6B;	// Image data type
	final static byte  TYPE_DESCRIPTIONS  	= (byte) 0x6C;	// Description data type
	final static byte  TYPE_LOG  			= (byte) 0x6D;	// Description data type
	
	final static short POSITION_CONSUMERID  = 200;
	final static short POSITION_POINTS_TOUCHED  = 220; 
	final static short POSITION_NEW_DATE = 240;
					
	private static byte[] selectResponse = { 
		(byte)0x6F,(byte)0x2C,
		   (byte)0x84,(byte)0x0B,(byte)0xA0,(byte)0x00,(byte)0x00,(byte)0x05,(byte)0x70,(byte)0x01,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x00,(byte)0x00,
		   (byte)0xA5,(byte)0x1D,
		       (byte)0x9F,(byte)0x12,(byte)0x0F,(byte)0x31,(byte)0x33,(byte)0x35,(byte)0x36,(byte)0x20,(byte)0x54,(byte)0x61,(byte)0x70,(byte)0x63,(byte)0x65,(byte)0x6e,(byte)0x74,(byte)0x69,(byte)0x76,(byte)0x65,
		       (byte)0x5F,(byte)0x25,(byte)0x04,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00, 
		       (byte)0x9F,(byte)0x08,(byte)0x01,(byte)0x83 }; //version number. high order bit indicates pre-release
	// 81 - changes to tags and lengths in campaign and also included most technical debt
	// 82 - Mostly editorial. Very minor changes to the RETRIEVE command to deal with empty command data.
	// 83 - Incorporates type 4 tag functionality in the package and updating of the NDEF (default is 1356mobile.com/welcome) based on 
	//		a url received in the campaign.

	// TODO initializes a default campaign. Must be removed 
	private static byte [] defaultCampaign = {
		(byte)0x6A,(byte)0x81,(byte)0x82,(byte)0x5F,(byte)0x25,(byte)0x04,(byte)0x11,(byte)0x22,(byte)0x33,(byte)0x44,(byte)0x9F,(byte)0x28,(byte)0x01,(byte)0x16,(byte)0xA2,(byte)0x75,(byte)0x87,(byte)0x01,(byte)0x01,(byte)0x88,(byte)0x02,(byte)0x27,(byte)0x10,(byte)0xA3,(byte)0x1F,(byte)0x89,(byte)0x02,(byte)0x00,(byte)0x01,(byte)0x8A,(byte)0x02,(byte)0x13,(byte)0x88,(byte)0x8B,(byte)0x01,(byte)0x01,(byte)0x80,(byte)0x0C,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x01,(byte)0x4C,(byte)0x04,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x13,(byte)0xA3,(byte)0x23,(byte)0x89,(byte)0x02,(byte)0x13,(byte)0x89,(byte)0x8A,(byte)0x02,(byte)0x26,(byte)0xAC,(byte)0x8B,(byte)0x01,(byte)0x01,(byte)0x80,(byte)0x0C,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x04,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x04,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x04,(byte)0x4C,(byte)0x04,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x91,(byte)0x9F,(byte)0x2A,(byte)0x01,(byte)0xFF,(byte)0xA3,(byte)0x26,(byte)0x89,(byte)0x02,(byte)0x26,(byte)0xAD,(byte)0x8A,(byte)0x02,(byte)0x27,(byte)0x10,(byte)0x8B,(byte)0x01,(byte)0x01,(byte)0x80,(byte)0x0C,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x07,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x07,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x07,(byte)0x4C,(byte)0x04,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x94,(byte)0x9F,(byte)0x2A,(byte)0x01,(byte)0xFF,(byte)0x8E,(byte)0x01,(byte)0x02	};
	
	private static byte[] SEID = { (byte)0x46,(byte)0x04, (byte)0x50,(byte)0x00, (byte)0x10,(byte)0x09 }; 
	private RandomData randomByte = null;	
	
	// demo keys
	public static byte [] keyData = {
		(byte)0x06, (byte)0x26, (byte)0x0f, (byte)0x5f, (byte)0xc3, (byte)0x34, (byte)0xc5, (byte)0xc2,
		(byte)0x1d, (byte)0x68, (byte)0x4a, (byte)0xfa, (byte)0xe7, (byte)0x63, (byte)0x49, (byte)0xd5 };	
	
	// Signature data container
		public static byte [] signature = {
			(byte)0x5f, (byte)0x40, (byte)0x08, (byte)0xff, (byte)0x00, (byte)0x00, (byte)0xfa, (byte)0xe7, (byte)0x63, (byte)0x49, (byte)0xd5 };
			
	// Initial value of nonce - value is junk at the moment
	public static byte [] nonce = {
		(byte)0x44, (byte)0x08, (byte)0xff, (byte)0x00, (byte)0x00, (byte)0xfa, (byte)0xe7, (byte)0x63, (byte)0x49, (byte)0xd5 };	

	// Space for the epoch
	public byte[] epoch = {	(byte)0x45, (byte)0x05, (byte)0x00, (byte)0x51, (byte)0xF8, (byte)0x69, (byte)0x68 };
	
	// TODO still have to determine the size of the log. Also add cycling to the code below
	public byte[] log = new byte[10000];
	
	// Assign 7000 bytes to hold a record of each consumer that have touched this SE within a period. That is, can keep a record
	// or 1000 consumers (6 byte id) + 1 byte to record the number of touches.
	public byte[] consumerLog = new byte[7000];
	
	// Assign 4000 bytes to hold a campaign. This will be populated with data received from the MSI.
	private static byte [] campaign = new byte [4000];
	
	// Assign 5000 bytes to hold an image. This will be populated with data received from the MSI.
	private static byte [] image = new byte [5000];
	
	// Assign 2000 bytes to hold descriptions. This will be populated with data received from the MSI.
	private static byte [] descriptions = new byte [2000];
	
	// Assign 5000 bytes for temporary storage. Large sets of data received from the MSI will be soted here until the signature can be verified at which
	// point the data will be moved to its intended location.
	private static byte [] temp = new byte [5000];
	
	// The date at which time the consumer log can be cleared
	public byte[] consumerLogExpiration = new byte[5];
	
	public short logRecords = 0;
	public short logOffset = 14;						// this offset accounts for the header
	public short highValueRecords = 0;
	
	short dataType = 0;
	short dataOffset = 0;
	short lengthOfData = 0; 
	short nextExpected = 0;
	short touchesAllowed = 0;
	boolean multiTap = false;

	
	private static DESKey doubleKey;
	private static Signature sign;
			
	public TapApplet(byte[] buffer, short offset, byte length) {
		randomByte = RandomData.getInstance(RandomData.ALG_SECURE_RANDOM);
		doubleKey = (DESKey) KeyBuilder.buildKey(KeyBuilder.TYPE_DES, KeyBuilder.LENGTH_DES3_2KEY, false);
		sign = Signature.getInstance(Signature.ALG_DES_MAC8_NOPAD, false);
		
		// Initialize the header data in the log array
		log[0] = (byte)0x6D;    // template tag
		log[1] = (byte)0x82;	// length is always assumed to be at 2 bytes.
		log[2] = (byte)0x00;
		log[3] = (byte)0x00;
		log[4] = (byte)0x5F;	// tag 5F70 - number of log records
		log[5] = (byte)0x70;
		log[6] = (byte)0x02;
		log[7] = (byte)0x00;	
		log[8] = (byte)0x00;	
		log[9] = (byte)0x5F;	// tag 5F71 - number of high value records in the file
		log[10] = (byte)0x71;
		log[11] = (byte)0x02;
		log[12] = (byte)0x00;	  
		log[13] = (byte)0x00;	
		
		// TODO initializes a default campaign. Must be removed 
		Util.arrayCopyNonAtomic(defaultCampaign, (short)0, campaign, (short)0, (short)133);
		
		register(buffer, offset, length) ;
	}
   
   public static void install( byte[] buffer, short offset, byte length )
   {
      length = (byte) buffer[offset];
      offset++;
      new TapApplet(buffer, offset, length) ;
   }		
   
   public void process( APDU apdu )
    {
        byte[] buffer = apdu.getBuffer();
        byte ins = buffer[ISO7816.OFFSET_INS];
        if ( ins == INS_PROCESS_SELECT ){
            processSelect(apdu);
            return;
        }
        if ( ins == INS_INFORMATION ){
            information(apdu);
            return;
        }
        if ( ins == INS_RETRIEVE ){
        	retrieveSecureElement(apdu);	            
            return;
        }
        if ( ins == INS_UPDATE ){
        	updateSecureElement(apdu);
            return;
        }
        if ( ins == INS_READ ){
        	readData(apdu);
            return;
        }
        if ( ins == INS_GET_RESPONSE ){
        	getResponse(apdu);
            return;
        }
        ISOException.throwIt( ISO7816.SW_INS_NOT_SUPPORTED ) ;
        return;
    }
	/**
	* 	Process the Select command
	* 	At the moment it is assumed that there will only be a single instance.
	*/    
    private void processSelect(APDU apdu) {

    	// Check that the select command is actually selecting this applet
    	if (!selectingApplet()) ISOException.throwIt(ISO7816.SW_FILE_NOT_FOUND);
	    	
    	byte[] apduBuffer = apdu.getBuffer();

    	if (apduBuffer[ISO7816.OFFSET_P1] != 0x04)
      		ISOException.throwIt(ISO7816.SW_INCORRECT_P1P2);
    	
    	// Reset data type 
    	dataType = 0;
    	
    	apdu.setIncomingAndReceive();
 
    	//populate the FCI response
    	short offset = 0;
      	Util.arrayCopy(selectResponse, (short)0, apduBuffer, offset, (short)selectResponse.length );
      	// Mover the current campign identifier into the response
      	if(campaign[0] != (byte)0x00){
      		short propOffset = findPositionInBuffer(apdu, (short)0, (short) 0x00a5, (byte)0);
      		short campaignOffset = findPositionInBuffer(apdu, (short)propOffset, (short) 0x5F25, (byte)0);
		    // find offset to the campaign ID in the campaign field
		    short campaignIDOffsetSE = findPositionInCampaign((short)0, (short) 0x5F25, (byte)0);
		    Util.arrayCopy(campaign, (short)campaignIDOffsetSE, apduBuffer, campaignOffset, (short)7 );
      	}
      	
    	apdu.setOutgoingAndSend ((short)0, (short)selectResponse.length);

        return;
    }
    
    /**
     * 	Retrieve prize related data from the Secure Element. 
     *  Will always return the SEID and Nonce in the 61 template
     *  INFORMATION command xx 20 xx xx xx [1 or 2 templates]
     *  Response is 1 or 2 templates and/or banner, image and description objects
     */
    private void information(APDU apdu) {
    	short levelAchieved = 1;
        byte[] buffer = apdu.getBuffer();

        apdu.setIncomingAndReceive();
    	// Check whether a campaign has been populated
    	if( campaign[0] == 0x00 ){
    		ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
    	}

        // find the consumer profile template within the 1st template. This order is forced. Consumer profile followed by breadcrumb, if present.
        short templateOffset = findPositionInBuffer(apdu, (byte) 03, (short) 0x66, (byte)0);
    	short profileOffset = findPositionInBuffer(apdu, (byte) templateOffset, (short) 0x67, (byte)0);
    	if(profileOffset == 0){
    		ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        	// throw an exception as the consumer application is not behaving correctly if a profile is not present and is not in the 1st template.
    	}
    	
    	// Copy signature into temp space so it does not get overwitten by padding. At the moment this is going into eeprom but could move it into 
    	// the buffer
    	short signatureOffset = findPositionInBuffer(apdu, templateOffset, (short)0x5f40, (byte)0);
    	Util.arrayCopyNonAtomic(buffer, signatureOffset, signature, (short)0, (short)11);   	
    	if(!verifySignature(apdu, profileOffset)){
//   for testing only 		signData(apdu, (short) profileOffset, (short)(26));
 // remove comment   		ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
    	}
    	
		// check if this campaign is part of a multi-tap campaign. 
    	short multiTapOffset = findPositionInCampaign((short)0, (short)0x00A1, (byte)0);
		if(multiTapOffset != (short)0){
			multiTap = true;					
		}
		
		// Check for number of touches allowed. if not present value will be set to zero
		touchesAllowed = findPositionInCampaign((short)0, (short)0x9F28, (byte)0);
		if(touchesAllowed>0){
			touchesAllowed = (short)campaign[touchesAllowed+3];
		}
		
        // Do date checking. If consumer profiles's date/time is greater, then this becomes the SE's date/time.
        short epochOffset = findPositionInBuffer(apdu, (byte) profileOffset, (short) 0x45, (byte)0);
        if(Util.arrayCompare(epoch, (short)0, buffer, epochOffset, (short)5) < 0){
        	Util.arrayCopyNonAtomic(buffer, epochOffset, epoch, (short)0, (short)7);
        	if(touchesAllowed > 0){
	        	// if the epoch is greater then the consumer log expiration, then clear out the log and calculate the new date.
	        	// As the consumer log expiration date is initially zero, the 1st interaction sets this date.
	        	if( Util.arrayCompare(epoch, (short)2, consumerLogExpiration, (short)0, (short)5) > 0 ){
	        		short retentionPeriod = findPositionInCampaign((short)0, (short) 0x9F29, (byte)0);
	        		if( retentionPeriod > 0 ){
	        			calculateNewDate(apdu, retentionPeriod);	
	        			Util.arrayCopyNonAtomic(buffer, POSITION_NEW_DATE, consumerLogExpiration, (short)0, (short)consumerLogExpiration.length);
	        			Util.arrayFillNonAtomic(consumerLog, (short)0, (short)7000, (byte)0x00);  
	        		} else {
	        			// will cause the log to never be cleared
	        			Util.arrayFillNonAtomic(consumerLogExpiration, (short)0, (short)5, (byte)0xFF); 
	        		}
	        	}
        	}
        } else {
        	// TODO possibly add a check in here to return an error to force the consumer to sync with the MSI.
        	// for example, check how old the profile date is and then if older than a time window (e.g. 1 week), force the sync.
        	// Also continuously reduce the time window if a newer epoch does not get received.
        	// The idea of keeping the date current from one touchpoint to the next using the profile
        	// may not be the best solution as it could make it simple to use skimmed
        	// profiles for a brute force/denial of service attack

        }
        
        // Temporarily store the consumer ID 
        short consumerIDOffset = findPositionInBuffer(apdu, (byte) profileOffset, (short) 0x5F50, (byte)0);
        Util.arrayCopy(buffer, (short)(consumerIDOffset), buffer, (short)POSITION_CONSUMERID, (short)9);
        
        // If a limited number of touches are allowed, check in the consumer log for the consumer's ID, if already present, increment number of 
        // touches and if the limit has been reached, throw an exception. 
        if(touchesAllowed > 0){
        	boolean found = false;
	        // check if this consumer has previously visited this touchpoint and if so whether the number of touches have been exceeded
	        for ( short i = 0; i<7000; i=(short)(i+7) ){
	        	if(Util.arrayCompare(buffer, (short)(POSITION_CONSUMERID + 3), consumerLog, (short) i, (short) 6) == 0 ){
	        		// Consumer has reached the limit of touches for this touchpoint
	        		if(touchesAllowed == consumerLog[i+6] ){
	        			// check if there is a specific message to be presented
	        			short messageID = findPositionInCampaign((short) 0, (short)0x004C, (byte)0);
	        			if( messageID != 0 ){
	        				Util.arrayCopy(campaign, messageID, buffer, (short)0, (short)6);
	        				apdu.setOutgoingAndSend((short)0, (short)6);
	        				return;
	        			} else {
	        				ISOException.throwIt(ISO7816.SW_RECORD_NOT_FOUND);
	        			}
	        		}
	        		found = true;
	        		byte tempTouches = consumerLog[i+6];
	        		tempTouches++;
	        		consumerLog[i+6] = tempTouches;	
	        		break;
	        	}
	        }
	        
	        // If the consumer ID is not in the consumer log, place in any random position in array. Even if this overwrites
	        // an existing record, this just gives that "lucky" consumer another set of touches to this touchpoint but should not
	        // be an issue. Cyclic would be worse as it would be predictable and a fraudster would know when their record had been 
	        // overwritten.
	        if( !found ) {
		    	randomByte.generateData( buffer, (short) 250, (short) 2 );
		    	short rnd;
		        rnd = Util.makeShort((byte)(buffer[250] & (byte)0x03), buffer[251]);
		        while ( rnd >= (short) 1000 ) rnd -= 1000;
		        Util.arrayCopyNonAtomic(buffer, (short)(POSITION_CONSUMERID + 3), consumerLog, (short) (rnd*7), (short) 6);    
		        consumerLog[rnd*7+6] = (byte)0x01;
	        }
        }
              
        // The breadcrumb template immediately follows the profile template. If present initially determine the level achieved
        // and the touchedpoints that have previously been touched 
        short breadCrumbOffset = 0;
        short sequenceOffset = 0;
        short messageOffset = 0;
        if (multiTap){        
        	sequenceOffset = findPositionInCampaign((short) multiTapOffset, (short)0x0081, (byte)0);
        	templateOffset = findPositionInBuffer(apdu, (byte) 03, (short) 0x0066, (byte)1);
        	breadCrumbOffset = findPositionInBuffer(apdu, (byte) templateOffset, (short) 0x69, (byte)0);
        	if( breadCrumbOffset != 0 ){
        		
	        	// Verify that the breadcrumb is still valid if a epoch is present. 
        		short expirationOffset = findPositionInBuffer(apdu, (byte) breadCrumbOffset, (short) 0x45, (byte)0);
        		if(expirationOffset != 0){
        			if( Util.arrayCompare(buffer, (short)(expirationOffset+2), epoch, (short)0, (short)5) < 0 ){
        				ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
        			}
        		}
        		
	        	// Copy signature into temp space so it does not get overwitten by padding. at the moment this is going into eeprom but could 
        		// move it into the buffer
	        	signatureOffset = findPositionInBuffer(apdu, templateOffset, (short)0x5f40, (byte)0);
	        	Util.arrayCopyNonAtomic(buffer, signatureOffset, signature, (short)0, (short)11);      	        	
	        	if(!verifySignature(apdu, breadCrumbOffset)){
	        		ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
	        	}
	    	
    			// If level is present, check what level has been achieved. 
    			short levelAchievedOffset = findPositionInBuffer(apdu, breadCrumbOffset, (short) 0x0087, (byte)0);
    			if( levelAchievedOffset != 0){
	    			levelAchieved = (short)(buffer[levelAchievedOffset+2]+1);
	    			buffer[levelAchievedOffset+2] = (byte) (levelAchieved);
    			}
    			
    			// Temporarily store touchpoints already visited in this campaign. This is an amalgamation of touchpoint sequences 
    			short touchedPointsOffset = findPositionInBuffer(apdu, (byte) breadCrumbOffset, (short) 0x0081, (byte)0);
    			if(touchedPointsOffset != 0 ){
    				short lengthOfObject = (short)(buffer[touchedPointsOffset+1]+2);
    				Util.arrayCopy(buffer, (short)(touchedPointsOffset), buffer, (short)POSITION_POINTS_TOUCHED, (short)lengthOfObject); 
    				// set length byte to zero and byte following to zero to cater for math related to forced order
    				buffer[POSITION_POINTS_TOUCHED+1] = (byte)0x00;
    				buffer[POSITION_POINTS_TOUCHED+lengthOfObject] = (byte)0x00;
    			}
    			
    			// if a sequence is present, determine within which byte the sequence bit is set   			
    			if(sequenceOffset != 0 ) {
	    			short length = (short)(campaign[sequenceOffset+1]);
	    			short i;
	        		for( i=2; i<(short)(i+length); i++ ){
	        			if(campaign[sequenceOffset+i] != (byte)0 ) break;
	        		}
	                // if order of touches is forced, check that this touchpoint's sequence bit is one higher than the last touchpoint tapped.
	                short forceOrderOffset = findPositionInCampaign((short) multiTapOffset, (short)0x0082, (byte)0);
	                short descriptionOffset = findPositionInCampaign((short) multiTapOffset, (short)0x4C, (byte)0);
	                if (forceOrderOffset != 0 && campaign[forceOrderOffset+2]==(byte)0xff){	                		
                		short original = Util.makeShort(buffer[POSITION_POINTS_TOUCHED+i], buffer[POSITION_POINTS_TOUCHED+i+1]);
                		short current = 0;
                		if(i==2){
                			current =  Util.makeShort((byte)(buffer[POSITION_POINTS_TOUCHED+i]|campaign[sequenceOffset+i]), buffer[POSITION_POINTS_TOUCHED+i+1]);
                		} else {
                			current =  Util.makeShort((byte)(buffer[POSITION_POINTS_TOUCHED+i]|campaign[sequenceOffset+i]), (byte)(buffer[POSITION_POINTS_TOUCHED+i+1]|campaign[sequenceOffset+i+1]));                			
                		}
                		if((short)(original*2) != current ) {
                			Util.arrayCopyNonAtomic(campaign, descriptionOffset, buffer, (short)0, (short)6);
                			apdu.setOutgoingAndSend((short)0, (short)6);
                			return;
                		}
	                } else {
	                	// check that a tag with the exact same sequence has not already been tapped
	                	if( campaign[sequenceOffset+i] == (byte)(campaign[sequenceOffset+i]&buffer[POSITION_POINTS_TOUCHED+i])){
                			Util.arrayCopyNonAtomic(campaign, descriptionOffset, buffer, (short)0, (short)6);
                			apdu.setOutgoingAndSend((short)0, (short)6);
                			return;
	                	}
	                }
    			}
        	} else {
        		// check if breadcrumb is required for this touchpoint to be part of a multitap campaign.
        		// as no breadcrumb is present, this touch cannot generate a breadcrumb
        		short breadCrumbRequiredOffset = findPositionInCampaign((short)multiTapOffset, (short)0x83, (byte)0);
        		if(campaign[breadCrumbRequiredOffset+2]==(byte)0xff){
        			multiTap = false;
        		}      
        	}
        	// Determine if there is a specific message to be presented for this multitap campaign.
        	messageOffset = findPositionInCampaign((short) multiTapOffset, (short)0x4C, (byte)0);           
        }
        
        // Find the correct payout level. Search through all levels looking for a level less than or equal to the level achieved and
        // with a matching profile, if any.
        short payoutStoredOffset = 0;
    	short i = 0;
    	// Look through a maximum of 20 levels. Should be more than enough and will break when no more levels are present
    	for(i=0; i<=20; i++){
        	short nextPayoutStored = findPositionInCampaign((short) 0, (short)0x00A2, (byte)i);
        	if(nextPayoutStored == 0) break;
        	// assume that the attribute is present. This will account for no attribute at this level. 
        	boolean attributePresent = true;
        	// check whether the level requires a consumer attribute and if so check if that attribute is present in the consumer profile
        	short attributeOffsetSE = findPositionInCampaign((short) nextPayoutStored, (short)0x008F, (byte)0);        	
        	if(attributeOffsetSE != 0){
        		// As an attribute is required, set to false until a match is found
        		attributePresent = false;
        		short ConsumerattributeOffsetP = findPositionInBuffer(apdu, (byte) profileOffset, (short) 0x00aa, (byte)0);
        		if( ConsumerattributeOffsetP != 0 ){
	        		short j = 0;
	        		// look through 20 attributes in the profile. Will break when no more attributes are present.
	        		for(j=0; j<20; j++){
	        			short attributeOffsetP = findPositionInBuffer(apdu, (byte) ConsumerattributeOffsetP, (short) 0x008f, (byte)j);
	        			if( attributeOffsetP == 0 ) break;
	        			if(Util.arrayCompare(campaign, attributeOffsetSE, buffer, attributeOffsetP, (short) 6) == 0 ){
	        				attributePresent = true;
	        			}	
	        		}        
        		}
        	}
        	// check level. Whether multi-tap or not, the current tap is an achievement and each payout template has at least a level of 1
        	// The current level will be ignored if the required attribute is not present in the consumer profile.
	        if( attributePresent == true )	{
	        	short payoutLevel = findPositionInCampaign((short) nextPayoutStored, (short)0x0087, (byte)0);	        	
	        	if( campaign[payoutLevel+2] <= levelAchieved ) {
	        		payoutStoredOffset = nextPayoutStored;
	        	}
	        }
    	}
    	
    	// assume that the range is 10000
    	short range = 10000;
    	// Determine if there is a specific range set for this payout level
        short rangeOffset = findPositionInCampaign((short) payoutStoredOffset, (short)0x0088, (byte)0);  
        if ( rangeOffset != 0 ){
        	range = Util.makeShort(campaign[rangeOffset+2], campaign[rangeOffset+3]);
        }    
        randomByte.generateData( buffer, (short) 250, (short) 2 );								// a short random number. 2 bytes
        short random = Util.makeShort( buffer[250], (byte)(buffer[251] + 1));     				// ensure that random value cannot be zero
        if(random < 0)random = (short)(random*-1);												// ensure that the random number is not negative
        while ( random > range ) random -= range;												// reduce the random number to less than or equal to range
        short lowerBound = 0;
        short upperBound = 0;
        boolean found = false;
        short payoutTier = 0;
        i = 0;
     // find  each tier and check whether the random number generated falls within lower and upper bounds
        while(!found){     	        	
    	    payoutTier = findPositionInCampaign((short) payoutStoredOffset, (short)0x00A3, (byte)i);	
    	    if(payoutTier == 0){
    	    	ISOException.throwIt(ISO7816.SW_RECORD_NOT_FOUND);								// This could only occur if the campaign is incorrectly formatted
    	    }
    	    i++;
    	    rangeOffset = findPositionInCampaign((short) payoutTier, (short)0x0089, (byte)0);
        	lowerBound = Util.makeShort(campaign[rangeOffset+2], campaign[rangeOffset+3]);
        	rangeOffset = findPositionInCampaign((short) payoutTier, (short)0x008A, (byte)0);
        	upperBound = Util.makeShort(campaign[rangeOffset+2], campaign[rangeOffset+3]);
        	if(random >= lowerBound && random <= upperBound){
        		// check whether there is a limited number of this prize to be awarded and if so decrement by 1.
        		// if this value has already been reached, downgrade to the level below
        		short maximum = findPositionInCampaign((short)payoutTier, (short)0x008E, (byte)0);
        		if( maximum != 0 ){
        			if(campaign[maximum+2] > (byte)0 ){
        				campaign[maximum+2] = (byte)(campaign[maximum+2]-1);
        				found = true;
        			} else {
        				// reduce the random number to less than the lower bound and search through the tiers again. it is possible that 
        				// these may not be in order and that the lower tier has also reached its maximum
        				random = (short)(lowerBound - 1);
        				// all lower tiers have reached the maximum.
        				if(random == 0){
        					ISOException.throwIt(ISO7816.SW_RECORD_NOT_FOUND);					// throw an exception	
        				}
        				i=0;
        			}
        		} else {
        			found = true;
        		}
        	}
        }
        
        // build the coupon
        buffer[0] = 0x66; 																					// template tag
		buffer[1] = 0x00; 
		buffer[2] = 0x68; 																					// coupon tag
		buffer[3] = 0x00; 
        short couponOffset = 4;
        couponOffset = Util.arrayCopy(SEID, (short)0, buffer, couponOffset, (short)SEID.length);  			// SEID 
        couponOffset = Util.arrayCopy(buffer, (short)POSITION_CONSUMERID, buffer, couponOffset, (short)9);  // Consumer ID 
        couponOffset = Util.arrayCopy(epoch, (short)0, buffer, couponOffset, (short)7);						// Epoch
        short campaignOffset = findPositionInCampaign((short) 0, (short)0x5F25, (byte)0);
        couponOffset = Util.arrayCopy(campaign, (short)campaignOffset, buffer, couponOffset, (short)7);		// Campaign ID
        // If a sequence offset is present in the campaign - determined above in multitap code.
        short sequenceLength = 0;
        if( sequenceOffset > 0 ){
        	sequenceLength = (short)(campaign[sequenceOffset+1]+2);
        	couponOffset = Util.arrayCopy(campaign, (short)sequenceOffset, buffer, couponOffset, sequenceLength);
        }
        // Build the Game info
        buffer[couponOffset++] = (byte)0xa3; 
        buffer[couponOffset++] = (byte)0x00;
        // maintain the current couponOffset and build a new offset for the game info. Use to determine the length of template
        short gameInfoOffet = couponOffset;									
        short offset = findPositionInCampaign((short) payoutTier, (short)0x008b, (byte)0);
        short length = (short)(campaign[offset+1]+2);
        gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);			// Game type
        offset = findPositionInCampaign((short) payoutTier, (short)0x0080, (byte)0);
        length = (short)(campaign[offset+1]+2);
        gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);			// game controls
        offset = findPositionInCampaign((short) payoutTier, (short)0x004C, (byte)0);
        length = (short)(campaign[offset+1]+2);
        gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);			// prize identifier
        offset = findPositionInCampaign((short) payoutTier, (short)0x9f2a, (byte)0);
        if( offset != 0 ) {
        	length = (short)(campaign[offset+2]+3);
        	gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);		// high value, if present
        }
        offset = findPositionInCampaign((short) payoutTier, (short)0x8d, (byte)0);
        if( offset != 0 ) {	
        	length = (short)(campaign[offset+1]+2);
        	gameInfoOffet = Util.arrayCopy(campaign, offset, buffer, gameInfoOffet, length);			// validity, if present
        }      
        buffer[couponOffset-1] = (byte)(gameInfoOffet-couponOffset);									// Length of a3 template       
        buffer[3] = (byte)(gameInfoOffet-4);															// - 4 accounts for both template and template length bytes
        couponOffset = gameInfoOffet;       
        // sign the coupon
        couponOffset =  signData(apdu, (short) 2, (short)(couponOffset-2));								// sign template 68 and its data and append the signature        
        buffer[1] = (byte)(couponOffset-2);																// -2 accounts for template 66 and length byte
        
        // Write the coupon to the log
        writeToLog (apdu, (short) 0, (short)couponOffset);
        
        if(multiTap){
            buffer[100] = 0x66; // template tag
    		buffer[101] = 0x00; 
    		buffer[102] = 0x69; // breadcrumb tag
    		buffer[103] = 0x00; 
            breadCrumbOffset = 104;
            buffer[breadCrumbOffset++] = (byte)0x45;
            buffer[breadCrumbOffset++] = (byte)0x05;
        	// this offset comes from building the coupon and points to the validity in game info
            calculateNewDate(apdu, offset);	
            breadCrumbOffset = Util.arrayCopy(buffer, POSITION_NEW_DATE, buffer, breadCrumbOffset, (short)5);			// calculated expiration date
            breadCrumbOffset = Util.arrayCopy(buffer, (short)POSITION_CONSUMERID, buffer, breadCrumbOffset, (short)9);  // Consumer ID 
            breadCrumbOffset = Util.arrayCopy(campaign, (short)campaignOffset, buffer, breadCrumbOffset, (short)7);		// Campaign ID
            // If this campaign supports sequences, OR this touchpoint sequence with the sequences - already touched - from the received breadcrumb
            if( sequenceOffset > 0 ){
            	for(i=0; i<sequenceLength; i++){
            		buffer[breadCrumbOffset++] = (byte)(campaign[sequenceOffset+i]|buffer[POSITION_POINTS_TOUCHED+i]);
            	}
            }
            buffer[breadCrumbOffset++] = (byte)0x87;									
            buffer[breadCrumbOffset++] = (byte)0x01;
            buffer[breadCrumbOffset++] = (byte)levelAchieved;												// Level achieved by touching this point
            
         // Build the Game info
            buffer[breadCrumbOffset++] = (byte)0xa3;
            buffer[breadCrumbOffset++] = (byte)0x00;
            // maintain the current couponOffset and build a new offset for the game info. Use to determine the length of template
            gameInfoOffet = breadCrumbOffset;									
            offset = findPositionInCampaign((short) payoutTier, (short)0x008b, (byte)0);
            length = (short)(campaign[offset+1]+2);
            gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);			// Game type
            offset = findPositionInCampaign((short) payoutTier, (short)0x0080, (byte)0);
            length = (short)(campaign[offset+1]+2);
            gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);			// game controls
            offset = findPositionInCampaign((short) payoutTier, (short)0x004C, (byte)0);
            length = (short)(campaign[offset+1]+2);
            gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);			// prize identifier
            offset = findPositionInCampaign((short) payoutTier, (short)0x9f2a, (byte)0);
            if( offset != 0 ) {
            	length = (short)(campaign[offset+2]+3);
            	gameInfoOffet = Util.arrayCopy(campaign, (short)offset, buffer, gameInfoOffet, length);		// high value, if present
            }
            offset = findPositionInCampaign((short) payoutTier, (short)0x8d, (byte)0);
            if( offset != 0 ) {	
            	length = (short)(campaign[offset+1]+2);
            	gameInfoOffet = Util.arrayCopy(campaign, offset, buffer, gameInfoOffet, length);			// validity, if present
            }            
            buffer[breadCrumbOffset-1] = (byte)(gameInfoOffet-breadCrumbOffset);							// Length of a3 template           
            buffer[103] = (byte)(gameInfoOffet-104);														// - 4 accounts for both template and template length bytes
         
            // Sign the breadcrumb
            breadCrumbOffset = gameInfoOffet;
            signData(apdu, (short) 102, (short)(breadCrumbOffset-102));
            breadCrumbOffset=(short) (breadCrumbOffset+11);
            buffer[101] = (byte)(breadCrumbOffset-102);            											// -2 accounts for template 66 and length byte
            couponOffset = Util.arrayCopy(buffer, (short)100, buffer, couponOffset, (short)(breadCrumbOffset-100));   // coupon offset is the current total length
        }
            
        // Check if any banners/static offers are present for the campaign and if so copy the banners template into the buffer
        short bannersOffset = findPositionInCampaign((short) 0, (short)0x00A9, (byte)0);
        if(bannersOffset != 0){       	
        	length = (short)(campaign[bannersOffset+1] + 2);											// assumes that the length is only one byte
        	couponOffset = Util.arrayCopy(campaign, bannersOffset, buffer, couponOffset, length); 
        }
        if(messageOffset != 0 ){
        	couponOffset = Util.arrayCopy(campaign, messageOffset, buffer, couponOffset, (short)6); 	// if a previous message has been identified
        }
        // Check if a image/brand identifier is present and if so copy into buffer
        short imageOffset = findPositionInCampaign((short) 0, (short)0x9f30, (byte)0);
        if(imageOffset != 0){
        	couponOffset = Util.arrayCopy(campaign, imageOffset, buffer, couponOffset, (short)6); 
        }
        apdu.setOutgoingAndSend((byte)0, couponOffset );
    }
    
    /**
     * Write coupon to the log
     * @param apdu
     * @param offset - from which to start writing
     * @param length - length of the data to write
     */
    // At the moment everything is being written. Not sure if it is worth the effort to save 6 bytes for the SEID
    // also if the file gets full, new coupons just overwrite from the beginning.
    // TODO have to think if there is a better way to handle this. May make sense to set a maximum common length for all coupons and 
    // then only overwrite low value coupons. once the number of high value records reach the maximum, stop interacting with consumer IDs
    private void writeToLog (APDU apdu, short offset, short length) {
    	byte[] buffer = apdu.getBuffer();
    	if((short)(logOffset+length) > 10000 ){
    		logOffset = 14;
    	}
    		
    	Util.arrayCopyNonAtomic(buffer, offset, log, logOffset, length);
    	logRecords++;
    	logOffset = (short)(logOffset+length);
    	
    }
    
    /**
     * Sign data in the buffer
     * @param apdu
     * @param offset - from which to start signing
     * @param length - length of the data to sign
     * @return The offset plus the appended signature data
     */
    private short signData (APDU apdu, short offset, short length ) {
    	byte[] buffer = apdu.getBuffer();
    	doubleKey.setKey(keyData, (short)0);
    	short paddedLength = length;
    	if(length%8 != 0){
    		short padding = (short)(8-(length%8));
    		short i = 0;
    		for(i=(short)(offset+length); i<(offset+length+padding); i++){
    			buffer[i] = (byte)0x00;
    		}
    		paddedLength = (short)(length + padding);
    	}
    	// No ICV
	    sign.init(doubleKey, Signature.MODE_SIGN);
		sign.sign(buffer, offset, paddedLength, buffer, (short)(offset+length+3));
		buffer[offset+length] = (byte)0x5f;
		buffer[offset+length+1] = (byte)0x40;
		buffer[offset+length+2] = (byte)0x08;
		// length and offset passed in don't account for the 66 template which is not included in the signature
		// so besides the signature data object add back 2 to the returned length
		return (short) (length+11+2);
    }
    
    /**
     * Verifies the signature applied across the received command data in the buffer. The received signature is stored in the
     * signature buffer
     * @param apdu
     * @param offset
     * @return True [if verified], false otherwise
     */
    private boolean verifySignature (APDU apdu, short offset ) {
    	byte[] buffer = apdu.getBuffer();
    	doubleKey.setKey(keyData, (short)0);
    	short length = (short)(buffer[offset+1]+2);
    	short paddedLength = length;
    	if(length%8 != 0){
    		short padding = (short)(8-(length%8));
    		short i = 0;
    		for(i=(short)(offset+length); i<(offset+length+padding); i++){
    			buffer[i] = (byte)0x00;
    		}
    		paddedLength = (short)(length + padding);
    	}
    
    	// No ICV
	    sign.init(doubleKey, Signature.MODE_VERIFY);
		return sign.verify(buffer, (short)(offset), paddedLength, signature, (short)3, (short)8);
    }
    
    /**
     * Verifies the signature applied across the data temporarily stored in the temp buffer. The received signature is stored in the
     * signature buffer
     * @param length
     * @return True [if verified], false otherwise
     */
    private boolean verifyTemp ( short length ) {
    	doubleKey.setKey(keyData, (short)0);
    	short paddedLength = length;
    	if(length%8 != 0){
    		short padding = (short)(8-(length%8));
    		paddedLength = (short)(length + padding);
    	}
    	
    	// No ICV
	    sign.init(doubleKey, Signature.MODE_VERIFY);
		return sign.verify(temp, (short)0, paddedLength, signature, (short)3, (short)8);
	
    }
/**
 * 	Retrieve signed data from the Secure Element. 
 *  Will always return the SEID and Nonce in the 61 template
 *  RETRIEVE command xx 24 xx xx [2 or 4] [5F70 and/or 5F71]
 *  Response is 61 template and a signature
 */   	    
    private void retrieveSecureElement(APDU apdu) {
    	
    	byte[] buffer = apdu.getBuffer();
    	
    	if(buffer[4] != (byte)0x00){
    		apdu.setIncomingAndReceive();
    	}
    	    	
    	// move the coupon counters into their respective positions in the log header 
		Util.setShort(log, (short)7, logRecords);
		Util.setShort(log, (short)12, highValueRecords);
		Util.setShort(log, (short)2, (short)(logOffset-4));
		
    	short offset = 20;
    	buffer[offset++] = (byte)0x61;
    	buffer[offset++] = (byte)0x0;
    	offset = Util.arrayCopy(SEID, (short)0, buffer, offset, (short)SEID.length);
    	offset = Util.arrayCopy(nonce, (short)0, buffer, offset, (short)nonce.length);
    	
    	short i = 5;
        while( buffer[i] != (byte)0x00){        	
        	if ((byte)(buffer[i]&(byte)0x1F) == (byte)0x1F){
    			i++;
    			short tag = Util.makeShort(buffer[i-1], buffer[i]);
    			if (tag == 0x5F70) {
    				offset = Util.arrayCopy(log, (short)4, buffer, offset, (short)5);
    			}
    			if (tag == 0x5F71) {
    				offset = Util.arrayCopy(log, (short)9, buffer, offset, (short)5);
    			}
        	}
    		i++;
    	}
        
        signData (apdu, (short) 20, (short)(offset-20) );
        
    	apdu.setOutgoingAndSend((short)20, (short)(offset-20+11));
 	
    }
    
/**
 * 	Update information on the Secure Element. The updating of the following information can be updated on receipt of the final command
 *  if the signature is valid
 *    - The Nonce, this information is always updated as it required
 *      - As part of the Nonce the Epoch is also updated
 *    - The Secure Element Identifier
 *    - The campaign information
 *    - The image information
 *    - The descriptions information
 *  UPDATE command xx 26 [00 or 80] [0..n] [1..n] xx....xx
 */	    
    private void updateSecureElement(APDU apdu) {
    	apdu.setIncomingAndReceive();
    	
    	byte[] buffer = apdu.getBuffer();
    	short sequence = (short)buffer[3];
    	
    	// first command in the sequence
    	if (sequence == 0){
			short positionOfSignature = findPositionInBuffer(apdu, (byte)3, (short)0x5F40, (byte)0);
			if(positionOfSignature!=0){
				// Store the signature for checking on the final command
				Util.arrayCopyNonAtomic(buffer, (short)(positionOfSignature), signature, (short)0, (short)11);
			} else {
				// output an error if signature is not present
				ISOException.throwIt( Util.makeShort((byte)0x69, (byte)0x82) );
			} 
			
    		short positionOfContainer = findPositionInBuffer(apdu, (byte)3, (short)0x61, (byte)0);
    		lengthOfData = (short)(Util.makeShort((byte)0, (byte)buffer[4]) - positionOfContainer + 5);
    		Util.arrayCopyNonAtomic(buffer, (short)(positionOfContainer), temp, (short)0, (short)lengthOfData);   		
    		// Check for a valid Nonce
			short positionOfNonce = findPositionInBuffer(apdu, (byte)positionOfContainer, (short)0x44, (byte)0);
			if(positionOfNonce!=0){
	    		// If 1st byte of received nonce is not 00, return error.
	    		if(buffer[positionOfNonce+2] != (byte)0x00) {
	    			ISOException.throwIt( Util.makeShort((byte)0x69, (byte)0x82) );
	    		}
		    	// Check that counter in Nonce is 1 greater than current counter
		    	if (buffer[positionOfNonce+4] != (byte)(nonce[4]+1)){
		    		if(buffer[positionOfNonce+4] != (byte)0 && buffer[positionOfNonce+3] != (byte)(nonce[3]+1)){
		    			// if nonce is incorrect, return error SW.
//   remove comment				        ISOException.throwIt( Util.makeShort((byte)0x69, (byte)0x83) );
		    		}
		    	}
			} else {
				// output an error if nonce is not present
				ISOException.throwIt( Util.makeShort((byte)0x69, (byte)0x82) );
			}			
			nextExpected = 1;
    	} else {
    		if(nextExpected != sequence ){
    			ISOException.throwIt( Util.makeShort((byte)0x69, (byte)0x82) );
    		}
    		Util.arrayCopyNonAtomic(buffer, (short)5, temp, (short)lengthOfData, (short)buffer[4]);
    		lengthOfData = (short)(lengthOfData+(short)buffer[4]); 		
    		nextExpected++;
    	}
    	if(buffer[2] == (byte)0x80 ){    		
    		
    		// determine the length of the data temporarily stored in the temp array.
    		short lengthOfTemplate;
    		short lengthOfHeader;  // tag plus length byte(s)
    		if(temp[1] == (byte)0x81 ){
        		lengthOfTemplate = (short)(Util.makeShort((byte)0x00, temp[2])+3);
        		lengthOfHeader = 3;
        	} else {
        		if(temp[1] == (byte)0x82 ){
        			lengthOfTemplate = (short)(Util.makeShort( temp[2], temp[3])+4);
        			lengthOfHeader = 4;
        		} else {
        			lengthOfTemplate = (short)(Util.makeShort( (byte)0x00, temp[1])+2);
        			lengthOfHeader = 2;
        		}
        	}
    		// TODO check that signature is valid. If not throw exception and don't move the data from temp storage to the actual position
    		if(!verifyTemp ( (short)(lengthOfTemplate+lengthOfHeader)) ) {
//   Remove comments 			ISOException.throwIt(ISO7816.SW_SECURITY_STATUS_NOT_SATISFIED);
    		}
      		// Find the nonce in the tag 61 and the Nonce's epoch becomes the new epoch
			short positionOfNonce = findPositionInTemp((byte)0, (short)0x44, (byte)0);
			
			Util.arrayCopyNonAtomic(temp, (short)(positionOfNonce+5), epoch, (short)2, (short)5);
			Util.arrayCopyNonAtomic(temp, (short)(positionOfNonce), nonce, (short)0, (short)10);
			
			// Find the SEID in the tag 61. If present this becomes the new SEID
			short positionOfSEID = findPositionInTemp((byte)0, (short)0x46, (byte)0);
			if(positionOfSEID!=0){
				Util.arrayCopyNonAtomic(temp, (short)(positionOfSEID), SEID, (short)0, (short)6);
			}
			// Find the number of log Records in the tag 61. If present the log data gets reset.
			short dataOffset = findPositionInTemp((byte)0, (short)0x5F70, (byte)0);
			if(dataOffset!=0){
				logRecords = 0;
				logOffset = 14;
				highValueRecords = 0;
			}
			// Find the Campaign Template in the tag 61. If present this will becomes the new Campaign Template
			dataOffset = findPositionInTemp((byte)0, (short)0x6A, (byte)0);
			if(dataOffset!=0){
				dataType = TYPE_CAMPAIGN;
			} else {
    			// Find the Image/Brand Template in the tag 61. If present this will becomes the new Brand Template
				dataOffset = findPositionInTemp((byte)0, (short)0x6B, (byte)0);
    			if(dataOffset!=0){
    				dataType = TYPE_IMAGE;
    			} else {
	    			// Find the Image/Brand Template in the tag 61. If present this will becomes the new Brand Template
    				dataOffset = findPositionInTemp((byte)0, (short)0x6C, (byte)0);
	    			if(dataOffset!=0){
	    				dataType = TYPE_DESCRIPTIONS;
	    			} else {
	    				ISOException.throwIt(ISO7816.SW_RECORD_NOT_FOUND);
	    			}
    			}
			}	

    		if(temp[dataOffset+1] == (byte)0x81 ){
        		lengthOfTemplate = (short)(Util.makeShort((byte)0x00, temp[dataOffset+2])+3);
        		lengthOfHeader = 3;
        	} else {
        		if(temp[dataOffset+1] == (byte)0x82 ){
        			lengthOfTemplate = (short)(Util.makeShort( temp[2], temp[dataOffset+3])+4);
        			lengthOfHeader = 4;
        		} else {
        			lengthOfTemplate = (short)(Util.makeShort( (byte)0x00, temp[dataOffset+1])+2);
        			lengthOfHeader = 2;
        		}
        	}
    		if(dataType == TYPE_DESCRIPTIONS){
    			Util.arrayCopyNonAtomic(temp, dataOffset, descriptions, (short)0, (short)(lengthOfTemplate+lengthOfHeader));
    		}
    		if(dataType == TYPE_IMAGE){
    			Util.arrayCopyNonAtomic(temp, dataOffset, image, (short)0, (short)(lengthOfTemplate+lengthOfHeader));
    		}
    		if(dataType == TYPE_CAMPAIGN){
    			
    			// Check whether the campaign ID has changed and if so clear out the consumer log. 
    			short campaignIDOffsetCurrent = findPositionInCampaign((short)0, (short)0x5f25, (byte)0);
    			short campaignIDOffsetNew = findPositionInTemp((short)dataOffset, (short)0x5f25, (byte)0);
    			if( Util.arrayCompare(campaign, campaignIDOffsetCurrent, temp, campaignIDOffsetNew, (short)7) != 0 ){
    				
        			// clear out the consumer log. 
        			Util.arrayFillNonAtomic(consumerLog, (byte)0, (short) 7000, (byte)0x00);
        			Util.arrayFillNonAtomic(consumerLogExpiration, (byte)0, (short) 5, (byte)0x00);
    			}
    			
    			// Check whether a new URL for the NDEF in Type 4 Tag is present.
    			short urlOffset = findPositionInTemp((short)dataOffset, (short)0x9f38, (byte)0);
    			if( urlOffset != 0 ){
    				short urlLength = (short)(temp[urlOffset+1]+2);
    				Util.arrayCopyNonAtomic(temp, urlOffset, Type4Tag.NDEF, (short) 7, urlLength);
    				Type4Tag.NDEF[4] = (byte)(urlLength+1); 		// + 1 to account for payload identification
    				Type4Tag.NDEF[1] = (byte)(urlLength+5);			// 
    				
    			}
    			Util.arrayCopyNonAtomic(temp, dataOffset, campaign, (short)0, (short)(lengthOfTemplate+lengthOfHeader));
    		}

    		Util.arrayFillNonAtomic(temp, (byte)0, (short) 5000, (byte)0x00);    		
		} 
    }
    
    /***
     * Read log, image, descriptions or a specific description data from the secure element
     * When reading the log or image - only the complete template can be retrieved
     * However for descriptions - as there are multiple - it is possible to read the complete template or a description with
     * a specific identifier. This is intended to make the interaction between the consumer app and the SE as quick as possible.
     * If the data being read is longer than 256 bytes, this will return the 1st 256 bytes and subsequent GET RESPONSE commands will
     * retrieve the remaining data 
     * READ apdu is xx B0 xx xx [01 or 06] [6B, 6C, 6D or 4C 04 xx xx xx xx]
     * 
     * @param apdu
     */
    private void readData(APDU apdu) {
    	boolean found = false;
    	short outputLength = 256;
    	apdu.setIncomingAndReceive();
    	
    	byte[] buffer = apdu.getBuffer();
    	if( buffer[4] == (byte)0x01){
	    	if( buffer[5] == TYPE_IMAGE){
	    		if(image[1] == (byte)0x81 ){
	    			lengthOfData = (short)(Util.makeShort((byte)0x00, image[2])+3);
	        	} else {
	        		if(image[1] == (byte)0x82 ){
	        			lengthOfData = (short)(Util.makeShort( image[2], image[3])+4);
	
	        		} else {
	        			lengthOfData = (short)(Util.makeShort( (byte)0x00, image[1])+2);
	        		}
	        	}	    		
	    		if(lengthOfData <= 256 ){
	    			outputLength = lengthOfData;
	    			lengthOfData = 0;
	    		} else {
	    			dataType = TYPE_IMAGE;
	    			lengthOfData = (short)(lengthOfData - 256);
	    			dataOffset = 256;
	    		}
	    		Util.arrayCopy(image, (short)0, buffer, (short)0, (short)outputLength);  		
	    	}
	    	if( buffer[5] == TYPE_LOG){
	    		// For the log file always set the length as 82xxxx
	        	lengthOfData = (short)(Util.makeShort( log[2], log[3])+4);	        	    		
	    		if(lengthOfData <= 256 ){
	    			outputLength = lengthOfData;
	    			lengthOfData = 0;
	    		} else {
	    			dataType = TYPE_LOG;
	    			lengthOfData = (short)(lengthOfData - 256);
	    			dataOffset = 256;
	    		}
	    		Util.arrayCopy(log, (short)0, buffer, (short)0, (short)outputLength);  		
	    	}
	    	if( buffer[5] == TYPE_DESCRIPTIONS){
	    		found = true;
	    		dataOffset = 0;	 		
	    	}
    	}
    	if( buffer[4] == (byte)0x06){
    		
    		byte i = 0;
    		while(!found){
    			dataOffset = findPositionInDescriptions((short)0, (short)0x00A4, (byte)i);
    			if(dataOffset == 0 ){
    				ISOException.throwIt(ISO7816.SW_RECORD_NOT_FOUND);
    			}
    			short idOffset = findPositionInDescriptions((short)dataOffset, (short)0x004C, (byte)0);
    			if( Util.arrayCompare(descriptions, (short)(idOffset+2), buffer, (short)7, (short)4) == 0 ){
    				found = true;
    			} else {
    				i++;
    			}
    		}
    		
    	}
    	if( found == true ){
	    	if(descriptions[dataOffset+1] == (byte)0x81 ){
				lengthOfData = (short)(Util.makeShort((byte)0x00, descriptions[dataOffset+2])+3);
	    	} else {
	    		if(descriptions[dataOffset+1] == (byte)0x82 ){
	    			lengthOfData = (short)(Util.makeShort( descriptions[dataOffset+2], descriptions[dataOffset+3])+4);
	
	    		} else {
	    			lengthOfData = (short)(Util.makeShort( (byte)0x00, descriptions[dataOffset+1])+2);
	    		}
	    	}
			if(lengthOfData <= 256 ){
				outputLength = lengthOfData;
				lengthOfData = 0;
			} else {
				dataType = TYPE_DESCRIPTIONS;
				lengthOfData = (short)(lengthOfData - 256);
				dataOffset = 256;
			}
			Util.arrayCopy(descriptions, (short)dataOffset, buffer, (short)0, (short)outputLength);
    	}
		apdu.setOutgoingAndSend((short)0, (short)outputLength);   

    	if(lengthOfData > 0){
			if (lengthOfData > 255){
				ISOException.throwIt(Util.makeShort((byte)0x61, (byte)0));
			} else {
				ISOException.throwIt(Util.makeShort((byte)0x61, (byte)lengthOfData));
			}
		} else {
			dataType = 0;
		}
    	
    }   
    
    /***
     * Process the GET RESPONSE apdu xx C0 xx xx xx
     * Only the INS byte is relevant. the content of all other header bytes are ignored
     * Assumes that apdu will only be issued as the result of a 61 xx response to a previous command
     * 
     * @param apdu
     */

    private void getResponse(APDU apdu) {
    	byte[] buffer = apdu.getBuffer();
    	short outputLength = 256;
    	if(lengthOfData <= 256 ){
			outputLength = lengthOfData;
			lengthOfData = 0;
		} else {
			lengthOfData = (short)(lengthOfData - 256);			
		}
    	if( dataType == TYPE_IMAGE ){   		
    		Util.arrayCopy(image, (short)dataOffset, buffer, (short)0, (short)outputLength);  
    	}
    	if( dataType == TYPE_DESCRIPTIONS ){
    		Util.arrayCopy(descriptions, (short)dataOffset, buffer, (short)0, (short)outputLength);
    	}
    	if( dataType == TYPE_LOG ){
    		Util.arrayCopy(log, (short)dataOffset, buffer, (short)0, (short)outputLength);
    	}
    	apdu.setOutgoingAndSend((short)0, (short)outputLength);   
    	if(lengthOfData > 0){
    		dataOffset = (short) (dataOffset + 256);
			if (lengthOfData > 255){
				ISOException.throwIt(Util.makeShort((byte)0x61, (byte)0));
			} else {				
				ISOException.throwIt(Util.makeShort((byte)0x61, (byte)lengthOfData));
			}
		} else {
			dataType = 0;
		}
    }
    
    
    /***
     * Locate the position of a tag within a template in the apdu. 
     * 
     * @param apdu
     * @param positionOfTemplate - within the apdu
     * @param tag - value to search for
     * @param occurrences - of the tag to search for. A value of 0 indicates the first occurrence 
     * 
     * @return  position of the tag found or zero if the tag is not found
     */
   
    private short findPositionInBuffer(APDU apdu, short positionOfTemplate, short tag, byte occurences){
    	byte timesFound = 0;
    	byte[] buffer = apdu.getBuffer();
    	short lengthOfTemplate = 0;
    	short offsetToData = 0;
    	// check whether the template length is 1, 2 or 3 bytes long
    	if(buffer[positionOfTemplate+1] == (byte)0x81 ){
    		lengthOfTemplate = Util.makeShort((byte)0x00, buffer[positionOfTemplate+2]);
    		offsetToData = 3;
    	} else {
    		if(buffer[positionOfTemplate+1] == (byte)0x82 ){
    			lengthOfTemplate = Util.makeShort( buffer[positionOfTemplate+2], buffer[positionOfTemplate+3]);
    			offsetToData = 4;
    		} else {
    			lengthOfTemplate = Util.makeShort( (byte)0x00, buffer[positionOfTemplate+1]);
    			offsetToData = 2;
    		}
    	}
    	// Loop through the template finding each tag (1 or 2 bytes) checking for a match to the passed in tag
    	// As the APDU buffer is only 255, if the tag is not found within the 255 bytes then the tag is not in the apdu.
    	if(lengthOfTemplate > 255 ) {
    		lengthOfTemplate = 255;
    	} 	
    	short i;
    	for (i = (short)(positionOfTemplate+offsetToData); i<=(short)(positionOfTemplate+offsetToData+lengthOfTemplate); ){
    		// Checks if the tag passed in matches a single length tag
    		if (Util.makeShort((byte)0, buffer[i]) == tag) {
    			// check that this is the correct occurrence, else skip over this tag and continue looking
    			if( occurences == timesFound ){
    				return i; 
    			} else {
    				timesFound++; 
    			}
    		}
    		// If a match was not found on a single byte tag, the following if will be passed over.
    		// however if the current tag found is a 2 byte tag, increase i by 1 to account for the extra byte
    		if ((byte)(buffer[i]&(byte)0x1F) == (byte)0x1F){
    			i++;
    			// Checks if the tag passed in matches a double length tag
    			if (Util.makeShort(buffer[i-1], buffer[i]) == tag) {	
    				// check that this is the correct occurrence, else skip over this tag and continue looking
    				if( occurences == timesFound ){
    					return (short)(i-1);
	    			} else {
	    				timesFound++;
	    			}
    			}
    		}
    		// increment i accommodating for a length of 1,2 or 3 bytes
    		if(buffer[i+1] == (byte)0x81 ){
        		i = (short)(i + 3 + Util.makeShort((byte)0x00, buffer[i+2]));
        	} else {
        		if(buffer[i+1] == (byte)0x82 ){
        			i = (short)(i + 4 + Util.makeShort( buffer[i+2], buffer[i+3]));
        		} else {
        			i = (short)(i + 2 + buffer[i+1]);
        		}
        	}
    	}
    	return 0;
    }
      
    /**
     * Locate the position of a tag within a template in the campaign storage. 
     * @param positionOfTemplate - of the template within the campaign storage
     * @param tag -value to search for
     * @param occurences - of the tag to search for. A value of 0 indicates the first occurrence
     * @return the position of the tag found or zero if the tag is not found
     */
    private short findPositionInCampaign( short positionOfTemplate, short tag, byte occurences ){
    	byte timesFound = 0;
    	short lengthOfTemplate = 0;
    	short offsetToData = 0;
    	// check whether the template length is 1, 2 or 3 bytes long
    	if(campaign[positionOfTemplate+1] == (byte)0x81 ){
    		lengthOfTemplate = Util.makeShort((byte)0x00, campaign[positionOfTemplate+2]);
    		offsetToData = 3;
    	} else {
    		if(campaign[positionOfTemplate+1] == (byte)0x82 ){
    			lengthOfTemplate = Util.makeShort( campaign[positionOfTemplate+2], campaign[positionOfTemplate+3]);
    			offsetToData = 4;
    		} else {
    			lengthOfTemplate = campaign[positionOfTemplate+1];
    			offsetToData = 2;
    		}
    	}
    	// Loop through the template finding each tag (1 or 2 bytes) checking for a match to the passed in tag
    	short i;
    	for (i = (short)(positionOfTemplate+offsetToData); i<=(short)(positionOfTemplate+offsetToData+lengthOfTemplate); ){
    		// Checks if the tag passed in matches a single length tag
    		if (Util.makeShort((byte)0, campaign[i]) == tag) {
    			// check that this is the correct occurrence, else skip over this tag and continue looking
    			if( occurences == timesFound ){
    				return i;
    			} else {
    				timesFound++; 
    			}
    		}
    		// If a match was not found on a single byte tag, the following if will be passed over.
    		// however if the current tag found is a 2 byte tag, increase i by 1 to account for the extra byte
    		if ((byte)(campaign[i]&(byte)0x1F) == (byte)0x1F){
    			i++;
    			// Checks if the tag passed in matches a double length tag
    			if (Util.makeShort(campaign[i-1], campaign[i]) == tag) {	
    				// check that this is the correct occurrence, else skip over this tag and continue looking
    				if( occurences == timesFound ){
    					return (short)(i-1);
	    			} else {
	    				timesFound++;
	    			}
    			}
    		}
    		// increment i accommodating for a length of 1,2 or 3 bytes
    		if(campaign[i+1] == (byte)0x81 ){
        		i = (short)(i + 3 + Util.makeShort((byte)0x00, campaign[i+2]));
        	} else {
        		if(campaign[i+1] == (byte)0x82 ){
        			i = (short)(i + 4 + Util.makeShort( campaign[i+2], campaign[i+3]));
        		} else {
        			i = (short)(i + 2 + campaign[i+1]);
        		}
        	}
    	}
    	return 0;
    }
    
    /**
     * Locate the position of a tag within a template in the campaign storage. 
     * @param positionOfTemplate - of the template within the campaign storage
     * @param tag -value to search for
     * @param occurences - of the tag to search for. A value of 0 indicates the first occurrence
     * @return the position of the tag found or zero if the tag is not found
     */
    private short findPositionInTemp( short positionOfTemplate, short tag, byte occurences ){
    	byte timesFound = 0;
    	short lengthOfTemplate = 0;
    	short offsetToData = 0;
    	// check whether the template length is 1, 2 or 3 bytes long
    	if(temp[positionOfTemplate+1] == (byte)0x81 ){
    		lengthOfTemplate = Util.makeShort((byte)0x00, temp[positionOfTemplate+2]);
    		offsetToData = 3;
    	} else {
    		if(temp[positionOfTemplate+1] == (byte)0x82 ){
    			lengthOfTemplate = Util.makeShort( temp[positionOfTemplate+2], temp[positionOfTemplate+3]);
    			offsetToData = 4;
    		} else {
    			lengthOfTemplate = temp[positionOfTemplate+1];
    			offsetToData = 2;
    		}
    	}
    	// Loop through the template finding each tag (1 or 2 bytes) checking for a match to the passed in tag
    	short i;
    	for (i = (short)(positionOfTemplate+offsetToData); i<=(short)(positionOfTemplate+offsetToData+lengthOfTemplate); ){
    		// Checks if the tag passed in matches a single length tag
    		if (Util.makeShort((byte)0, temp[i]) == tag) {
    			// check that this is the correct occurrence, else skip over this tag and continue looking
    			if( occurences == timesFound ){
    				return i;
    			} else {
    				timesFound++; 
    			}
    		}
    		// If a match was not found on a single byte tag, the following if will be passed over.
    		// however if the current tag found is a 2 byte tag, increase i by 1 to account for the extra byte
    		if ((byte)(temp[i]&(byte)0x1F) == (byte)0x1F){
    			i++;
    			// Checks if the tag passed in matches a double length tag
    			if (Util.makeShort(temp[i-1], temp[i]) == tag) {	
    				// check that this is the correct occurrence, else skip over this tag and continue looking
    				if( occurences == timesFound ){
    					return (short)(i-1);
	    			} else {
	    				timesFound++;
	    			}
    			}
    		}
    		// increment i accommodating for a length of 1,2 or 3 bytes
    		if(temp[i+1] == (byte)0x81 ){
        		i = (short)(i + 3 + Util.makeShort((byte)0x00, temp[i+2]));
        	} else {
        		if(temp[i+1] == (byte)0x82 ){
        			i = (short)(i + 4 + Util.makeShort( temp[i+2], temp[i+3]));
        		} else {
        			i = (short)(i + 2 + temp[i+1]);
        		}
        	}
    	}
    	return 0;
    }
    
    /**
     * Locate the position of a tag within a template in the descriptions storage. 
     * @param positionOfTemplate - of the template within the descriptions storage
     * @param tag -value to search for
     * @param occurences - of the tag to search for. A value of 0 indicates the first occurrence
     * @return the position of the tag found or zero if the tag is not found
     */
    // TODO check if this can be condensed as it is only called for 2 purposes.
    private short findPositionInDescriptions( short positionOfTemplate, short tag, byte occurences ){
    	byte timesFound = 0;
    	short lengthOfTemplate = 0;
    	short offsetToData = 0;
    	// check whether the template length is 1, 2 or 3 bytes long
    	if(descriptions[positionOfTemplate+1] == (byte)0x81 ){
    		lengthOfTemplate = Util.makeShort((byte)0x00, descriptions[positionOfTemplate+2]);
    		offsetToData = 3;
    	} else {
    		if(descriptions[positionOfTemplate+1] == (byte)0x82 ){
    			lengthOfTemplate = Util.makeShort( descriptions[positionOfTemplate+2], descriptions[positionOfTemplate+3]);
    			offsetToData = 4;
    		} else {
    			lengthOfTemplate = descriptions[positionOfTemplate+1];
    			offsetToData = 2;
    		}
    	}
    	// Loop through the template finding each tag (1 or 2 bytes) checking for a match to the passed in tag
    	short i;
    	for (i = (short)(positionOfTemplate+offsetToData); i<=(short)(positionOfTemplate+offsetToData+lengthOfTemplate); ){
    		// Checks if the tag passed in matches a single length tag
    		if (Util.makeShort((byte)0, descriptions[i]) == tag) {
    			// check that this is the correct occurrence, else skip over this tag and continue looking
    			if( occurences == timesFound ){
    				return i;
    			} else {
    				timesFound++; 
    			}
    		}
    		// If a match was not found on a single byte tag, the following if will be passed over.
    		// however if the current tag found is a 2 byte tag, increase i by 1 to account for the extra byte
    		if ((byte)(descriptions[i]&(byte)0x1F) == (byte)0x1F){
    			i++;
    			// Checks if the tag passed in matches a double length tag
    			if (Util.makeShort(descriptions[i-1], descriptions[i]) == tag) {	
    				// check that this is the correct occurrence, else skip over this tag and continue looking
    				if( occurences == timesFound ){
    					return (short)(i-1);
	    			} else {
	    				timesFound++;
	    			}
    			}
    		}
    		// increment i accommodating for a length of 1,2 or 3 bytes
    		if(descriptions[i+1] == (byte)0x81 ){
        		i = (short)(i + 3 + Util.makeShort((byte)0x00, descriptions[i+2]));
        	} else {
        		if(descriptions[i+1] == (byte)0x82 ){
        			i = (short)(i + 4 + Util.makeShort( descriptions[i+2], descriptions[i+3]));
        		} else {
        			i = (short)(i + 2 + descriptions[i+1]);
        		}
        	}
    	}
    	return 0;
    }
    
	/**
	 * Adds a validity period to the current epoch. 
	 * @param apdu
	 * @param offset -  of the validity period, the new calculated date replaces the validity period in the buffer 
	 */
	private void calculateNewDate(APDU apdu, short offset){
		
		short tempDate;
		byte[] buffer = apdu.getBuffer();
		tempDate = (short)(epoch[4]+campaign[offset+7]);
		if( tempDate <=256 ){
			buffer[POSITION_NEW_DATE+4] = (byte)(tempDate-256);
			buffer[POSITION_NEW_DATE+3] = (byte)(0);
		} else {
			buffer[POSITION_NEW_DATE+3] = (byte)(1);
		}
		tempDate = (short)(epoch[3]+campaign[offset+6]+buffer[POSITION_NEW_DATE+3]);
		if( tempDate <=255 ){
			buffer[POSITION_NEW_DATE+3] = (byte)(tempDate-256);
			buffer[POSITION_NEW_DATE+2] = (byte)(0);
		} else {
			buffer[POSITION_NEW_DATE+2] = (byte)(1);
		}
		tempDate = (short)(epoch[2]+campaign[offset+5]+buffer[POSITION_NEW_DATE+2]);
		if( tempDate <=255 ){
			buffer[POSITION_NEW_DATE+2] = (byte)(tempDate-256);
			buffer[POSITION_NEW_DATE+1] = (byte)(0);
		} else {
			buffer[POSITION_NEW_DATE+1] = (byte)(1);
		}
		tempDate = (short)(epoch[1]+campaign[offset+4]+buffer[POSITION_NEW_DATE+1]);
		if( tempDate <=255 ){
			buffer[POSITION_NEW_DATE+1] = (byte)(tempDate-256);
			buffer[POSITION_NEW_DATE+0] = (byte)(0);
		} else {
			buffer[POSITION_NEW_DATE+0] = (byte)(1);
		}
		tempDate = (short)(epoch[0]+campaign[offset+3]+buffer[POSITION_NEW_DATE+0]);

	}


}